/**
 * Created by Claudio on 12.12.2016.
 */

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.hash.HashFunction;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.BitSet;

public class BloomFilter {
    private int m, k;
    private HashFunction[] hashFunctions;
    private BitSet bitSet;

    public static void main(String[] args) {
        double p = 0.01;
        FileHandler fh = new FileHandler();
        try {
            ArrayList<String> words = fh.initWords("words");
            BloomFilter bloom = new BloomFilter(words.size(), p);
            System.out.println();
            System.out.println("Start adding word to filter");
            bloom.add(words.toArray(new String[words.size()]));
            System.out.println("Words added successfully");
            //test filter with no added words
            System.out.println("Start checking non added words");
            words = fh.initWords("test_filter");
            int c = 0;
            for (String word : words) {
                if (bloom.check(word)) c++;
            }
            double accuracy = (double) c / words.size();
            System.out.println("Checked words: " + words.size());
            System.out.println("Wrong checked: " + c);
            System.out.println("Defined accuracy: " + p);
            System.out.println("Effective accuracy: " + accuracy);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor
     * @param n number of expected elements
     * @param p error probability, in percentage
     */
    public BloomFilter(int n, double p) {
        System.out.println("Creating new Bloom filter");
        set_m_k(n, p);
        hashFunctions = new HashFunction[k];
        for (int i = 0; i < k; i++) {
            int seed = (int)(System.nanoTime() % Integer.MAX_VALUE);
            hashFunctions[i] = Hashing.murmur3_128(seed);
        }
        bitSet = new BitSet(m);
        System.out.println("Bloom filter created successfully");
    }

    /**
     * Defines m: the number of bits and k: the number of hash functions
     * @param n number of expected elements
     * @param p error probability, in percentage
     */
    private void set_m_k(int n, double p) {
        m = (int)Math.ceil(-n * Math.log(p) / Math.pow(Math.log(2), 2));
        k = (int)Math.ceil((double)m / (double)n * Math.log(2));
        System.out.println("Defined expected elements: " + n);
        System.out.println("Defined error probability: " + p);
        System.out.println("Calculated number of bits: " + m);
        System.out.println("Calculated number of hash functions: " + k);
    }

    public void add(@NotNull String... words) {
        for (String word : words) {
//            System.out.print("Add @" + word + " indices:   ");
            for (HashFunction function : hashFunctions) {
                HashCode code = function.newHasher().putBytes(word.getBytes()).hash();
                bitSet.set(Math.abs(code.asInt()) % m);
//                System.out.print((Math.abs(code.asInt()) % m) + "   ");
            }
//            System.out.print("\n");
        }
    }

    /**
     * Checks if the bloom filter may contains this word
     * @param word Word to check
     * @return
     */
    public boolean check(@NotNull String word) {
        boolean c = true;
        int i = 0;
        while (c && i < k) {
            HashCode code = hashFunctions[i].newHasher().putBytes(word.getBytes()).hash();
            c = bitSet.get(Math.abs(code.asInt()) % m);
            i++;
        }

//        String cStr = c ? "true" : "false";
        if (c) System.out.println("Check @" + word + " returns " + "true");
        return c;
    }
}
