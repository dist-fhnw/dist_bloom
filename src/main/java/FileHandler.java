import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Claudio on 12.12.2016.
 */
public class FileHandler {
    public static final String folder = "/";

    public ArrayList<String> initWords(String name)
            throws URISyntaxException, FileNotFoundException, IOException {
        File file = new File(getClass().getResource(folder + name + ".txt").toURI());
        if (!file.exists()) throw new FileNotFoundException();
        ArrayList<String> words = new ArrayList<String>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            br.lines().forEach(words::add);
        }
        return words;
    }
}
